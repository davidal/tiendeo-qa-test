
# Prueba QA
Este proyecto contiene las soluciones de las pruebas 2 y 3.

##### Automatización de UI Tests
Uso de Selenium y Behave con Python.  
El directorio del proyecto se encuentra en: `solutions/ui_tests`  
Para ejecutar el proyecto:
```sh
$ cd solutions/ui_tests
$ docker build -t ui_tests . && docker run -it -v /dev/shm:/dev/shm ui_tests bash
$ source run.sh
```

##### Automatización de Integration tests
Uso de Karate.  
El directorio del proyecto se encuentra en: `solutions/integration_tests`  
Para ejecutar el proyecto:
```sh
$ cd solutions/integration_tests
$ docker build -t karatest . && docker run -it karatest bash
$ mvn test -Dtest=IntegrationTest
```
