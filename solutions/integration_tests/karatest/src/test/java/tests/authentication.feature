Feature: Authentication feature

  Scenario: Get guest session id
    * def apiKey = "0f3e249b55143b2388b6c6c1fdba6900"
    * def guestSessionUrl = "https://api.themoviedb.org/3/authentication/guest_session/new?api_key=" + apiKey

    Given url guestSessionUrl
    And header Content-Type = 'application/json'
    When method GET
    Then status 200
