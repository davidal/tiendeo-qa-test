Feature: Integration rest testing

  Scenario Outline: Get top rated movies
    * def topRatedUrl = 'https://api.themoviedb.org/3/movie/top_rated?api_key=' + apikey

    Given url topRatedUrl
    And param page = <page>
    When method GET
    Then assert status == <status>
    And match response == <expected>

    Examples:
      | status! | apikey!                          | page! | expected!                                                                                             |
      | 200     | 0f3e249b55143b2388b6c6c1fdba6900 | 1     | {page: "#number", total_pages: "#number", results: "#array", total_results: "#number" }               |
      | 401     | TEST                             | 1     | {status_message: "Invalid API key: You must be granted a valid key.", status_code: 7, success: false} |
      | 422     | 0f3e249b55143b2388b6c6c1fdba6900 | 0     | {errors: "#array"}                                                                                    |

  Scenario Outline: Post rate movie
    * def authenticationResult = call read('../authentication.feature')
    * def sessionId = authenticationResult.response["guest_session_id"]
    * def rateMovieUrl = "https://api.themoviedb.org/3/movie/" + movie + "/rating?api_key=" + apikey

    Given url rateMovieUrl
    And header Content-Type = "application/json;charset=utf-8"
    And param guest_session_id = sessionId
    When request { value: '#(value)' }
    And method POST
    Then assert status == <status>
    And match response == <expected>

    Examples:
      | status! | apikey!                          | movie! | value! | expected!                                                                                                       |
      | 201     | 0f3e249b55143b2388b6c6c1fdba6900 | 674382 | 5.50   | {status_code: 1, status_message: "Success.", success: true}                                                     |
      | 400     | 0f3e249b55143b2388b6c6c1fdba6900 | 754433 | 5.2    | {status_code: 18, status_message: "Value invalid: Values must be a multiple of 0.50.", success: false}          |
      | 400     | 0f3e249b55143b2388b6c6c1fdba6900 | 754433 | 10.5   | {status_code: 18, status_message: "Value too high: Value must be less than, or equal to 10.0.", success: false} |
      | 400     | 0f3e249b55143b2388b6c6c1fdba6900 | 754433 | -0.5   | {status_code: 18, status_message: "Value too low: Value must be greater than 0.0.", success: false}             |
      | 400     | 0f3e249b55143b2388b6c6c1fdba6900 | 1402   | Test   | {status_code: 18, status_message: "#string", success: false}                                                    |
      | 401     | 0f3e249b55143b2388b6c6c1fdba6900 | ocho   | 5.50   | {status_code: 34, status_message: "The resource you requested could not be found.", success: false}             |
      | 401     | TEST                             | 337401 | 5.50   | {status_code: 7, status_message: "Invalid API key: You must be granted a valid key.", success: false}           |

