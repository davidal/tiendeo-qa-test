package tests.rest;

import com.intuit.karate.junit5.Karate;

class RestRunner {
    
    @Karate.Test
    Karate testRest() {
        return Karate.run("rest").relativeTo(getClass());
    }    

}
