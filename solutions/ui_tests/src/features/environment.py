from selenium import webdriver


def before_all(context):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--window-size=1420,1080')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    context.browser = webdriver.Chrome(chrome_options=chrome_options)


def after_all(context):
    context.browser.quit()
