Feature: Ui testing Tiendeo's home page

  Scenario: Verify that a catalog item is accessible
    Given a user with web chrome browser on tiendeo home page
    When it clicks on element
    Then the element page is opened

  Scenario Outline: Verify that it is possible to write in the product text field and perform a search
    Given a user with web chrome browser on tiendeo home page
    When it writes "<product_text>" on product text field
    And it hits enter
    Then "<product_text>" appears on the title text

    Examples:
      | product_text |
      | Tablet       |

  Scenario Outline: Verify that it is possible to write in the city text field and perform a search
    Given a user with web chrome browser on tiendeo home page
    When it writes "<location_text>" on location text field
    And it hits enter
    Then "<location_text>" appears on the top offers text

    Examples:
      | location_text |
      | Barcelona     |

  Scenario Outline: Verify that a search is possible by writting values ​​in both fields
    Given a user with web chrome browser on tiendeo home page
    When it writes "<product_text>" on product text field
    And it writes "<location_text>" on location text field
    And it hits enter
    Then "<product_text>" appears on the title text

    Examples:
      | product_text | location_text |
      | Ropa         | Valencia      |


  Scenario Outline: Verify that a search is possible by clicking on the search button
    Given a user with web chrome browser on tiendeo home page
    When it writes "<product_text>" on product text field
    And it writes "<location_text>" on location text field
    And it clicks on search button
    Then "<product_text>" appears on the title text

    Examples:
      | product_text | location_text |
      | Relojes      | Sevilla       |
