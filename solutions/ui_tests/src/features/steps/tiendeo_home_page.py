from behave import given, when, then
from src.pages import TiendeoHomePage, SearchedElementPage, CatalogPage
import time


@given(u'a user with web chrome browser on tiendeo home page')
def step_impl(context):
    context.tiendeo_home_page = TiendeoHomePage()
    context.tiendeo_home_page.get_page(context.browser)


@when(u'it writes "{location_text}" on location text field')
def step_impl(context, location_text):
    context.button_location = context.tiendeo_home_page.get_city_text_field(
        context.browser)
    context.button_location.send_keys(location_text.lower())


@when(u'it hits enter')
def step_impl(context):
    context.tiendeo_home_page.press_enter(context.button_location)


@then(u'"{location_text}" appears on the top offers text')
def step_impl(context, location_text):
    assert context.tiendeo_home_page.check_title_top_offers(context.browser)
    assert location_text.lower() in context.tiendeo_home_page.get_title_top_offers(
        context.browser).lower()


@ then(u'"{location_text}" appears on the title text')
def step_impl(context, location_text):
    context.searched_element_page = SearchedElementPage()
    assert context.searched_element_page.check_title_element_city(
        context.browser)
    context.location_title = context.searched_element_page.get_title_element_city(
        context.browser)
    assert location_text.lower() in context.location_title.lower()


@ when(u'it writes "{product_text}" on product text field')
def step_impl(context, product_text):
    context.button_location = context.tiendeo_home_page.get_product_text_field(
        context.browser)
    context.button_location.send_keys(product_text.lower())


@ when(u'it clicks on search button')
def step_impl(context):
    context.tiendeo_home_page.click_on_search_button(context.browser)


@ when(u'it clicks on element')
def step_impl(context):
    context.first_product = context.tiendeo_home_page.get_products_list(
        context.browser)
    context.first_product[0].click()


@ then(u'the element page is opened')
def step_impl(context):
    context.browser.switch_to.window(context.browser.window_handles[-1])
    context.catalog_page = CatalogPage()
    assert context.catalog_page.check_close_button(context.browser)
    context.catalog_page.click_close_button(context.browser)
    context.browser.switch_to.window(context.browser.window_handles[0])
