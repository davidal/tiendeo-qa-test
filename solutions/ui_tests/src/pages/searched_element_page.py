from src.pom import Base, locators


class SearchedElementPage(Base):

    def check_title_element_city(self, driver):
        return self.check_element(
            driver, locators["searched_element_page"]["title_element_city"])

    def get_title_element_city(self, driver):
        return self.find_element(driver, locators["searched_element_page"]["title_element_city"]).get_attribute("textContent")
