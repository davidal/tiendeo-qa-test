from .tiendeo_home_page import TiendeoHomePage
from .catalog_page import CatalogPage
from .searched_element_page import SearchedElementPage
