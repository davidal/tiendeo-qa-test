from src.pom import Base, locators


class CatalogPage(Base):

    def check_close_button(self, driver):
        return self.check_element(driver, locators["catalog_page"]["close_page"])

    def click_close_button(self, driver):
        self.find_element(
            driver, locators["catalog_page"]["close_page"]).click()
