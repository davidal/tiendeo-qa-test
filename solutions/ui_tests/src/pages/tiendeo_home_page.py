from src.pom import BaseKeys, Base, locators
import time


class TiendeoHomePage(Base, BaseKeys):
    url = 'https://www.tiendeo.com/'

    def get_page(self, driver):
        driver.get(self.url)

    def get_title_page(self, driver):
        return driver.title

    def get_title_top_offers(self, driver):
        return self.find_element(driver, locators["tiendeo_home_page"]["title_top_offers"]).get_attribute('textContent')

    def get_city_text_field(self, driver):
        return self.find_element(driver, locators["tiendeo_home_page"]["city_text_field"])

    def get_product_text_field(self, driver):
        return self.find_element(driver, locators["tiendeo_home_page"]["product_text_field"])

    def get_products_list(self, driver):
        products_list = self.find_element(
            driver, locators["tiendeo_home_page"]["products_list"])
        products_list_items = self.find_elements(
            products_list, locators["tiendeo_home_page"]["products_list_items"])
        return products_list_items

    def check_search_button(self, driver):
        return self.check_element(driver, locators["tiendeo_home_page"]["search_button"])

    def check_title_top_offers(self, driver):
        return self.check_element(driver, locators["tiendeo_home_page"]["title_top_offers"])

    def click_on_search_button(self, driver):
        button = self.find_element(
            driver, locators["tiendeo_home_page"]["search_button"])
        time.sleep(0.5)
        button.click()
        time.sleep(0.5)
