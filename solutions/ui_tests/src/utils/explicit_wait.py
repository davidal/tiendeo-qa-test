from datetime import datetime


def explicit_wait(func, time, *args):
    start = datetime.now()
    element = None
    while (datetime.now() - start).total_seconds() < time:
        try:
            element = func(args[0], args[1])
            if element:
                break
        except:
            pass
    return element if element != None else func(args[0], args[1])
