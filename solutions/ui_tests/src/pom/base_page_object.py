from src.utils import explicit_wait


class Base:
    def find_element(self, driver, search_values):
        return driver.find_element(search_values[0], search_values[1])

    def find_elements(self, driver, search_values):
        return driver.find_elements(search_values[0], search_values[1])

    def check_element(self, driver, search_values, wait=30):
        try:
            is_open = explicit_wait(
                driver.find_element, wait, search_values[0], search_values[1])
            return True if is_open else False
        except:
            return False
