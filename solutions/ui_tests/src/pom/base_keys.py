from selenium.webdriver.common.keys import Keys
import time


class BaseKeys:
    keys = {
        "return": Keys.RETURN,
    }

    def press_enter(self, driver):
        self.__press(driver, self.keys["return"])

    def __press(self, driver, key):
        time.sleep(1)
        driver.send_keys(key)
        time.sleep(1)
