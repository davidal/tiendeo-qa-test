from selenium.webdriver.common.by import By


locators = {
    "tiendeo_home_page": {
        "product_text_field": (By.ID, "header-search-input"),
        "city_text_field": (By.ID, "header-city-input"),
        "title_top_offers": (By.XPATH, '//*[@id="allOffersSection"]/header/h1'),
        "search_button": (By.ID, "header-search-submit"),
        "products_list": (By.XPATH, '//*[@id="allOffersSection"]/div/ul'),
        "products_list_items": (By.TAG_NAME, 'li')
    },
    "catalog_page": {
        "close_page": (By.ID, 'close_button')
    },
    "searched_element_page": {
        "title_element_city": (By.XPATH, '//*[@id="pagetype_offers_product"]/div[1]/section[2]/div[2]/h1')
    }
}
